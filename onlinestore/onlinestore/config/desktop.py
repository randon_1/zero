# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Online Store",
			"color": "yellow",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Online Store")
		}
	]
